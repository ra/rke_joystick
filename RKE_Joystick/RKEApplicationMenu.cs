﻿using System;
using UnityEngine;

namespace RKE_Joystick
{
    public class RKEApplicationMenu : MonoBehaviour
    {

        private Rect windowPosition;
        private bool _hovering = false;
        private bool _stillHovering = false;
        private bool windowShowing = false;
        private bool captured = false;
        private RKEState state;

        private CustomKeyGUI customKeyGUI;
        private GUIStyle windowStyle;
        private GUIStyle labelStyle;
        private GUIStyle buttonStyle;

        public RKEApplicationMenu()
        {
            windowPosition.y = 38;
        }

        public bool hovering {
            set {
                _hovering = value;
                if (value)
                {
                    _stillHovering = value;
                }
            }
        }

        public void init(RKEState state)
        {
            this.state = state;
            customKeyGUI.init(state);

        }

        public void Awake() {
            windowStyle = new GUIStyle(HighLogic.Skin.window);
//            windowStyle.fontSize = 11;
//            windowStyle.alignment = TextAnchor.UpperLeft;
            windowStyle.padding.top = 5;
            windowStyle.padding.bottom = 2;

            labelStyle = new GUIStyle(HighLogic.Skin.label);
            labelStyle.fontSize = 11;
            labelStyle.padding = new RectOffset(0,0,0,0);
            labelStyle.margin = new RectOffset(1,1,1,0);
//            labelStyle.contentOffset = new Vector2(0,0);

            buttonStyle = new GUIStyle(HighLogic.Skin.button);
            buttonStyle.fontSize = 11;

            customKeyGUI = gameObject.AddComponent<CustomKeyGUI>();

        }
        public void Update() {
        }

        private void OnGUI()
        {
            if (Event.current.type == EventType.Repaint)
            {
                Rect captureRegion = windowPosition;
//                captureRegion.y -= 1; // grace
//                captureRegion.height += 2;
//                Debug.Log("h:" + _stillHovering + " c:" + captured + " w:" + windowShowing + " contains:" + captureRegion.Contains(Event.current.mousePosition)
//                    + " cr:" + captureRegion + " m:"+Event.current.mousePosition); 
                if (_stillHovering || captured || windowShowing)
                {
                    captured = captureRegion.Contains(Event.current.mousePosition);
                }
                windowShowing = _stillHovering || captured;

                _stillHovering = _hovering;
            }

            if (!(_stillHovering || captured))
            {
                return;
            }
            windowShowing = true;

            GUI.skin = HighLogic.Skin;


            windowPosition.x = Screen.width/2 + this.transform.parent.position.x - 19;
            windowPosition = JoystickMod.constrainToScreen(windowPosition);

            windowPosition = GUILayout.Window(this.GetInstanceID(), windowPosition, this.onWindow, "", windowStyle);

            GUI.skin = null;
        }

        void onWindow(int id)
        {
            try {
                GUILayout.BeginVertical(GUILayout.MinWidth(130)); {
                    GUILayout.Label("RKE Joystick:", labelStyle);
                    string toggleMsg = "Enabled";
                    if (state.bindingStr != null && state.bindingStr != "") {
                        toggleMsg += " ( " + state.bindingStr + " )";
                    }

                    if (RKEGUILayout.StateButton(state.windowVisible, new GUIContent(toggleMsg), null, Color.green, buttonStyle)) {
                        state.windowVisible = !state.windowVisible;
                    }
                    if (RKEGUILayout.MicroButton(false, "CHANGE KEYBINDING", XKCDColors.OffWhite, XKCDColors.OffWhite)) {
                        customKeyGUI.show(state.bindingStr, new Vector2(windowPosition.x, windowPosition.yMax));
                    }

                    GUILayout.Label("Fullscreen HUD:", labelStyle);
                    if (RKEGUILayout.StateButton(state.fullscreen, new GUIContent("HUD enabled"), null, Color.green, buttonStyle)) {
                        state.fullscreen = !state.fullscreen;
                    }
                    GUILayout.Label("HUD size:", labelStyle);
                    state.fullscreenSize = GUILayout.HorizontalSlider(state.fullscreenSize, 0.3f, 0.9f, GUILayout.ExpandWidth(true));
                    GUILayout.Label("HUD transparency:", labelStyle);
                    state.minTransparency = GUILayout.HorizontalSlider(state.minTransparency, 1, 0, GUILayout.ExpandWidth(true));
                    GUILayout.Label("HUD deadzone:", labelStyle);
                    state.fullscreenDeadRadius = GUILayout.HorizontalSlider(state.fullscreenDeadRadius, 0, 60, GUILayout.ExpandWidth(true));



                }
                GUILayout.EndVertical();
            }
            catch (Exception e) {
                JoystickMod.LogEventError("onWindow", e);
            }
        }
    }
}

