﻿using System;
using UnityEngine;

namespace RKE_Joystick
{
    public class PatchedAxisBinding_Single : AxisBinding_Single {
        static string inactiveIdTag = new AxisBinding_Single().idTag;
        KeyBinding keyPos;
        KeyBinding keyNeg;
        float kbScale;
        string prevIdTag;
        float prevNeutralPoint;

        public PatchedAxisBinding_Single(KeyBinding keyPos, KeyBinding keyNeg, float kbScale, AxisBinding_Single orig) {
            this.idTag = orig.idTag;
            this.name = orig.name;
            this.sensitivity = orig.sensitivity;
            this.deadzone = orig.deadzone;
            this.scale = orig.scale;
            this.inverted = orig.inverted;
            this.neutralPoint = orig.neutralPoint;
            this.switchState = orig.switchState;
            this.useSwitchState = orig.useSwitchState;
            this.inputLockMask = orig.inputLockMask;

            prevIdTag = idTag;

            this.keyPos = keyPos;
            this.keyNeg = keyNeg;
            this.kbScale = kbScale;
        }

        public float GetTotalAxisValue()
        {
            if (keyNeg.GetKey())
            {
                return -1 * kbScale;
            }
            if (keyPos.GetKey())
            {
                return 1 * kbScale;
            }
            return GetAxis();
        }

//        public override float GetAxis() {
////            return super.getAxis();
//        }

        public void setValue(float v) {
            if (v == 0f)
            {
                idTag = prevIdTag;
                neutralPoint = prevNeutralPoint;
            }
            else
            {
                prevIdTag = idTag;
                prevNeutralPoint = neutralPoint;

                idTag = inactiveIdTag;
                neutralPoint = v;
            }
        }
    }

    public class EVAControlState
    {
        static PatchedAxisBinding_Single patch(ref AxisBinding existing, KeyBinding keyPos, KeyBinding keyNeg, float kbScale) {
            if (existing.primary is PatchedAxisBinding_Single)
            {
                return (PatchedAxisBinding_Single)existing.primary;
            }
            PatchedAxisBinding_Single p = new PatchedAxisBinding_Single(keyPos, keyNeg, kbScale, existing.primary);
            existing.primary = p;
            return p;
        }

        PatchedAxisBinding_Single jetpack_x;
        PatchedAxisBinding_Single jetpack_y;
        PatchedAxisBinding_Single jetpack_z;
        PatchedAxisBinding_Single _yaw;

        public EVAControlState()
        {
            jetpack_x = patch(
                ref GameSettings.axis_EVA_translate_x,
                GameSettings.EVA_Pack_right,
                GameSettings.EVA_Pack_left, 1);
            jetpack_y = patch(
                ref GameSettings.axis_EVA_translate_y,
                GameSettings.EVA_Pack_down,
                GameSettings.EVA_Pack_up, -1);
            jetpack_z = patch(
                ref GameSettings.axis_EVA_translate_z,
                GameSettings.EVA_Pack_back,
                GameSettings.EVA_Pack_forward, -1);
            jetpack_z = patch(
                ref GameSettings.axis_EVA_translate_z,
                GameSettings.EVA_Pack_back,
                GameSettings.EVA_Pack_forward, -1);
            _yaw = patch(
                ref GameSettings.axis_EVA_yaw,
                GameSettings.EVA_yaw_right,
                GameSettings.EVA_yaw_left, 1);
        }

        public void resetForFlyit()
        {
            X = 0;
            Y = 0;
            Z = 0;
            yaw = 0;
        }

        public float X {
            set { jetpack_x.setValue(value); }
            get { 
                return jetpack_x.GetTotalAxisValue();
            }
        }
        public float Y {
            set { jetpack_y.setValue(value); }
            get { 
                return jetpack_y.GetTotalAxisValue();
            }
        }
        public float Z {
            set { jetpack_z.setValue(value); }
            get { 
                return jetpack_z.GetTotalAxisValue();
            }
        }
        public float yaw {
            set { _yaw.setValue(value); }
            get { 
                return _yaw.GetTotalAxisValue();
            }
        }
    }
}

