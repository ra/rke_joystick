using System;
using System.Collections.Generic;
using UnityEngine;

namespace RKE_Joystick
{
	public abstract class StickAxis {
		protected KeyBinding kbPos;
		protected KeyBinding kbNeg;
        public string posName;
        public string negName;
        protected bool supportsKeyLimiting;

        protected abstract void set(FlightCtrlState state, EVAControlState estate, float v);
		public abstract void setTrim(float x);

        public abstract float get(FlightCtrlState state, EVAControlState estate);
		public abstract float trim(FlightCtrlState state);

        bool activeInStaging;
        bool activeInLinDocking;
        bool activeInRotDocking;

		public StickAxis(KeyBinding kbPos, KeyBinding kbNeg,
            string posName, string negName,
            bool supportsKeyLimiting = true,
            bool activeInStaging = true, bool activeInLinDocking = true, bool activeInRotDocking = true) 
        {
			this.kbPos = kbPos;
			this.kbNeg = kbNeg;
            this.posName = posName;
            this.negName = negName;
            this.activeInStaging = activeInStaging;
            this.activeInLinDocking = activeInLinDocking;
            this.activeInRotDocking = activeInRotDocking;
            this.supportsKeyLimiting = supportsKeyLimiting;
		}

        string getKeyName(KeyBinding kb, string name)
        {
            if (FlightUIModeController.Instance.Mode == FlightUIMode.DOCKING)
            {
                if ((activeInLinDocking && !InputBinding.linRotState) ||
                    (activeInRotDocking && InputBinding.linRotState))
                {
                    return name + " " + kb.name ;
                }
                return name;
            }
            else
            {
                if (activeInStaging)
                {
                    return name + " " + kb.name;
                }
                else
                {
                    return name;
                }
            }
        }

		public string negKey()
		{
            return getKeyName(kbNeg, negName);
		}
		public string posKey()
		{
            return getKeyName(kbPos, posName);
		}

        public virtual bool isUserCommanding(FlightCtrlState state, EVAControlState estate)
        {
            return Mathfx.Approx(get(state, estate), trim(state), 0.1F) ? false : true;
        }

        public void setInto(FlightCtrlState state, EVAControlState estate, float v, bool overrideUser) {
			bool userCommanding = isUserCommanding(state, estate);
			if (overrideUser || !userCommanding) {
				v = Mathf.Clamp(v, -1f, 1f);
				set(state, estate, v);
			}
		}
		public float getKeyPos() {
			if (kbPos.GetKey()) {
				return 1f;
			}
			if (kbNeg.GetKey()) {
				return -1f;
			}
			return 0f;
		}

        public bool handleKeyPos(FlightCtrlState state, EVAControlState estate, float step)
		{
            if (!supportsKeyLimiting)
            {
                return false;
            }
            if (!kbPos.GetKey() && !kbNeg.GetKey())
            {
                // they aren't pressing this key, no need to limit
                return false;
            }
			float current = get(FlightGlobals.ActiveVessel.ctrlState, estate);
			if (Mathf.Abs(current) > step) {
				setInto(state, estate, Mathf.Sign(current) * step, true);
				return true;
			}
			return false;
		}
	}
	public class PitchAxis : StickAxis {
		public PitchAxis() : base(GameSettings.PITCH_UP, GameSettings.PITCH_DOWN, "PITCH UP", "PITCH DOWN",
        activeInLinDocking: false) 
        {
		}
        protected override void set(FlightCtrlState state, EVAControlState estate, float v)
		{
			state.pitch = v;
		}
        public override float get(FlightCtrlState state, EVAControlState estate) {
			return state.pitch;
		}
		public override float trim(FlightCtrlState state) {
			return state.pitchTrim;
		}
		public override void setTrim(float v) {
			FlightInputHandler.state.pitchTrim = v;
		}
	}
	public class YawAxis : StickAxis {
		public YawAxis() : base(GameSettings.YAW_RIGHT, GameSettings.YAW_LEFT, "YAW RIGHT", "YAW LEFT", 
            activeInLinDocking: false) {
		}

        protected override void set(FlightCtrlState state, EVAControlState estate, float v)
		{
			state.yaw = v;
		}
        public override float get(FlightCtrlState state, EVAControlState estate) {
			return state.yaw;
		}
		public override float trim(FlightCtrlState state) {
			return state.yawTrim;
		}
		public override void setTrim(float v) {
			FlightInputHandler.state.yawTrim = v;
		}
	}
	public class RollAxis : StickAxis {
		public RollAxis() : base(GameSettings.ROLL_RIGHT, GameSettings.ROLL_LEFT, "ROLL RIGHT", "ROLL LEFT",
            activeInLinDocking: true) {
		}
        protected override void set(FlightCtrlState state, EVAControlState estate, float v)
		{
			state.roll = v;
		}
        public override float get(FlightCtrlState state, EVAControlState estate) {
			return state.roll;
		}
		public override float trim(FlightCtrlState state) {
			return state.rollTrim;
		}
		public override void setTrim(float v) {
			FlightInputHandler.state.rollTrim = v;
		}
	}
	public class TranslateZAxis : StickAxis {
		// Z: -1/1 FWD/BACK H/N
		public TranslateZAxis() : base(GameSettings.TRANSLATE_BACK, GameSettings.TRANSLATE_FWD, "BACK", "FWD") {
		}
        protected override void set(FlightCtrlState state, EVAControlState estate, float v)
		{
			state.Z = v;
		}
        public override float get(FlightCtrlState state, EVAControlState estate) {
			return state.Z;
		}
		public override float trim(FlightCtrlState state) {
			return 0f;
		}
		public override void setTrim(float v) {
		}
	}
	public class TranslateYAxis : StickAxis {
		// Y: -1/1 up/down I/K
		public TranslateYAxis() : base(GameSettings.TRANSLATE_DOWN, GameSettings.TRANSLATE_UP, "DOWN", "UP") {
		}
        protected override void set(FlightCtrlState state, EVAControlState estate, float v)
		{
			state.Y = -v;
		}
        public override float get(FlightCtrlState state, EVAControlState estate) {
			return -state.Y;
		}
		public override float trim(FlightCtrlState state) {
			return 0f;
		}
		public override void setTrim(float v) {
		}
	}
	public class TranslateXAxis : StickAxis {
		// X: 1/-1 left/right J/K
		public TranslateXAxis() : base(GameSettings.TRANSLATE_RIGHT, GameSettings.TRANSLATE_LEFT, "RIGHT", "LEFT") {
		}
        protected override void set(FlightCtrlState state, EVAControlState estate, float v)
		{
			state.X = -v;
		}
        public override float get(FlightCtrlState state, EVAControlState estate) {
			return -state.X;
		}
		public override float trim(FlightCtrlState state) {
			return 0f;
		}
		public override void setTrim(float v) {
		}
	}
    public class RoverXAxis : StickAxis {
        public RoverXAxis() : 
        base(GameSettings.WHEEL_STEER_RIGHT, GameSettings.WHEEL_STEER_LEFT, "RIGHT", "LEFT",
            activeInStaging: false, activeInLinDocking: true, activeInRotDocking: false) 
        {
        }
        protected override void set(FlightCtrlState state, EVAControlState estate, float v)
        {
            state.wheelSteer = -v;
        }
        public override float get(FlightCtrlState state, EVAControlState estate) {
            return -state.wheelSteer;
        }
        public override float trim(FlightCtrlState state) {
            return -state.wheelSteerTrim;
        }
        public override void setTrim(float v) {
            FlightInputHandler.state.wheelSteerTrim = -v;
        }
    }
    public class RoverYAxis : StickAxis {
        public RoverYAxis() : base(GameSettings.WHEEL_THROTTLE_DOWN, GameSettings.WHEEL_THROTTLE_UP, "BACK", "FWD",
            activeInStaging: false, activeInLinDocking: true, activeInRotDocking: false) {
        }
        protected override void set(FlightCtrlState state, EVAControlState estate, float v)
        {
            state.wheelThrottle = -v;
        }
        public override float get(FlightCtrlState state, EVAControlState estate) {
            return -state.wheelThrottle;
        }
        public override float trim(FlightCtrlState state) {
            // unlike every other axis, seems wheel throttle trim isn't set
            // into the passed in state, it's only to be had globally
            return -FlightInputHandler.state.wheelThrottleTrim;
        }
        public override void setTrim(float v) {
            FlightInputHandler.state.wheelThrottleTrim = -v;
        }
    }
    public class EVAPackXAxis : StickAxis {
        public EVAPackXAxis() : base(                
            GameSettings.EVA_Pack_right,
            GameSettings.EVA_Pack_left,
            "RIGHT", "LEFT",
            supportsKeyLimiting: false)
        {
        }
        protected override void set(FlightCtrlState state, EVAControlState estate, float v)
        {
            estate.X = v;
        }
        public override float get(FlightCtrlState state, EVAControlState estate) {
            return estate.X;
        }
        public override float trim(FlightCtrlState state) {
            return 0f;
        }
        public override void setTrim(float v) {
        }
    }
    public class EVAPackYAxis : StickAxis {
        public EVAPackYAxis() : base(                
            GameSettings.EVA_Pack_down,
            GameSettings.EVA_Pack_up,
            "DOWN", "UP",
            supportsKeyLimiting: false)
        {
        }
        protected override void set(FlightCtrlState state, EVAControlState estate, float v)
        {
            estate.Y = -v;
        }
        public override float get(FlightCtrlState state, EVAControlState estate) {
            return -estate.Y;
        }
        public override float trim(FlightCtrlState state) {
            return 0f;
        }
        public override void setTrim(float v) {
        }
    }
    public class EVAZPackAxis : StickAxis {
        public EVAZPackAxis() : base(                
            GameSettings.EVA_Pack_back,
            GameSettings.EVA_Pack_forward,
            "BACK", "FORWARD",
            supportsKeyLimiting: false)
        {
        }
        protected override void set(FlightCtrlState state, EVAControlState estate, float v)
        {
            estate.Z = -v;
        }
        public override float get(FlightCtrlState state, EVAControlState estate) {
            return -estate.Z;
        }
        public override float trim(FlightCtrlState state) {
            return 0f;
        }
        public override void setTrim(float v) {
        }
    }
//    public class EVAWalkYAxis : StickAxis {
//        public EVAWalkYAxis() : base(                
//            GameSettings.EVA_back,
//            GameSettings.EVA_forward,
//            supportsKeyLimiting: false)
//        {
//        }
//        protected override void set(FlightCtrlState state, EVAControlState estate, float v)
//        {
//            estate.Y = -v;
//        }
//        public override float get(FlightCtrlState state, EVAControlState estate) {
//            return -estate.Y;
//        }
//        public override float trim(FlightCtrlState state) {
//            return 0f;
//        }
//        public override void setTrim(float v) {
//        }
//    }
    public class EVAYawAxis : StickAxis {
        public EVAYawAxis() : base(                
            GameSettings.EVA_yaw_right,
            GameSettings.EVA_yaw_left,
            "YAW RIGHT", "YAW LEFT",
            supportsKeyLimiting: false)
        {
        }
        protected override void set(FlightCtrlState state, EVAControlState estate, float v)
        {
            estate.yaw = v;
        }
        public override float get(FlightCtrlState state, EVAControlState estate) {
            return estate.yaw;
        }
        public override float trim(FlightCtrlState state) {
            return 0f;
        }
        public override void setTrim(float v) {
        }
    }

	public enum JoystickPaneMode {
		PITCHYAW, PITCHROLL, TRANSLATE, ROVER, EVA_PACK
        //, EVA_WALK
	}

    class CursorInfo {
        public TextureCursor[] cursors;
        public StickAxis pitch;
        public StickAxis yaw;
        public StickAxis roll;

        public CursorInfo(TextureCursor[] cursors, 
            StickAxis pitch = null, StickAxis yaw = null, StickAxis roll = null)
        {
            this.cursors = cursors;
            this.pitch = pitch;
            this.yaw = yaw;
            this.roll = roll;
        }
        
    }
    class Cursors {
        public CursorInfo xy;
        public CursorInfo x2;
        public CursorInfo y2;

        public Cursors(   
            CursorInfo xy = null,
            CursorInfo x2 = null,
            CursorInfo y2 = null)
        {
            this.xy = xy;
            this.x2 = x2;
            this.y2 = y2;
        }
    }

	public struct StickValue {
		public bool active;
		public float v;
	}

	public class UserInputState {
		public StickValue x;
		public StickValue y;
		public StickValue x2;
		public StickValue y2;
		public bool alt;
	}

	public class JoystickPane
	{
		private static readonly StickAxis PITCH = new PitchAxis();
        private static readonly StickAxis YAW = new YawAxis();
        private static readonly StickAxis ROLL = new RollAxis();
        private static readonly StickAxis T_X = new TranslateXAxis();
        private static readonly StickAxis T_Y = new TranslateYAxis();
        private static readonly StickAxis T_Z = new TranslateZAxis();
        private static readonly StickAxis ROVER_X = new RoverXAxis();
        private static readonly StickAxis ROVER_Y = new RoverYAxis();
        private static readonly StickAxis EVA_X = new EVAPackXAxis();
        private static readonly StickAxis EVA_Y = new EVAPackYAxis();
        private static readonly StickAxis EVA_Z = new EVAZPackAxis();
        private static readonly StickAxis EVA_YAW = new EVAYawAxis();
//        private static readonly StickAxis EVA_WALK_Y = new EVAWalkYAxis();

        private static readonly StickAxis[] ALL_AXIS = { 
            PITCH, YAW, ROLL, T_X, T_Y, T_Z, ROVER_X, ROVER_Y, 
            EVA_X, EVA_Y, EVA_Z, EVA_YAW
            //, EVA_WALK_Y
        };

        Dictionary<JoystickPaneMode, Cursors> cursorMap = new Dictionary<JoystickPaneMode, Cursors>();

		private JoystickPaneMode paneMode;
		
		private StickAxis xStick;
		private StickAxis yStick;
		private StickAxis x2Stick;
		private StickAxis y2Stick;
		private float keyboardLimit = 1f;
//		private float deadRadius = 5.0f;
        private Rect xyRect = new Rect();
		private Rect x2Rect = new Rect();
		private Rect y2Rect = new Rect();

		private UserInputState input;

        private bool wereWeControlling = false;
        private bool restoreSAS = false;

        private RKEState ourstate;
        public Vector2 minsize;
        float animStartTime;
        float? animEndTime = null;
        float guiAlpha = 1;

		public JoystickPane()
		{
			input = new UserInputState();

            cursorMap.Add(JoystickPaneMode.PITCHYAW,
                new Cursors(
                    xy: new CursorInfo(new TextureCursor[] { MyResources.cursor_1, MyResources.cursor_2, MyResources.cursor_3 },
                        pitch: PITCH, yaw: YAW, roll: ROLL
                    ),
                    x2: new CursorInfo(new TextureCursor[] { MyResources.cursor_1 },
                        roll: ROLL
                    )
                ));
            cursorMap.Add(JoystickPaneMode.PITCHROLL,
                new Cursors(
                    xy: new CursorInfo(new TextureCursor[] { MyResources.cursor_1, MyResources.cursor_2, MyResources.cursor_3 },
                        pitch: PITCH, yaw: YAW, roll: ROLL
                    ),
                    x2: new CursorInfo(new TextureCursor[] { MyResources.cursor_fwd },
                        roll: YAW
                    )
                ));
            cursorMap.Add(JoystickPaneMode.TRANSLATE, 
                new Cursors(
                    xy: new CursorInfo(new TextureCursor[] { MyResources.cursor_1},
                        roll: ROLL),
                    x2: new CursorInfo(new TextureCursor[] { MyResources.cursor_1 },
                        roll: ROLL),
                    y2: new CursorInfo(new TextureCursor[] { MyResources.cursor_fwd })
                ));
            cursorMap.Add(JoystickPaneMode.ROVER,
                new Cursors(
                    xy: new CursorInfo(new TextureCursor[] { MyResources.cursor_rover_2 },
                        roll: ROVER_X
                    ),
                    x2: new CursorInfo(new TextureCursor[] { MyResources.cursor_rover_1 },
                        roll: ROLL
                    ),
                    y2: new CursorInfo(new TextureCursor[] { MyResources.cursor_rover_2 }
                    )
                ));

            cursorMap.Add(JoystickPaneMode.EVA_PACK,
                new Cursors(
                    xy: new CursorInfo(new TextureCursor[] { MyResources.cursor_eva_1}
                    ),
                    // don't show X2, because EVA_YAW doesn't seem to work properly
//                    x2: new CursorInfo(new TextureCursor[] { MyResources.cursor_eva_2 },
//                        roll: EVA_YAW
//                    ),
                    y2: new CursorInfo(new TextureCursor[] { MyResources.cursor_eva_2 }
                    )
                ));

		}
        public void init(RKEState state) {
            ourstate = state;
            setStickMode(JoystickPaneMode.PITCHYAW);
            setDisplayMode();
        }

		public void setKeyboardLimit(float v) {
			keyboardLimit = v;
		}
		public float getKeyboardLimit() {
			return keyboardLimit;
		}

        public bool IsEVA()
        {
            return paneMode == JoystickPaneMode.EVA_PACK;
        }
        public bool IsManualOverride() {
            return wereWeControlling && restoreSAS;
        }
		public JoystickPaneMode getStickMode() {
			return paneMode;
		}
        public void setStickMode(JoystickPaneMode mode) {
            bool switchToStagingMode = true;

			this.paneMode = mode;
			if (mode == JoystickPaneMode.TRANSLATE) {
				this.xStick = T_X;
				this.yStick = T_Y;
				this.x2Stick = ROLL;
				this.y2Stick = T_Z;
			} else if (mode == JoystickPaneMode.PITCHYAW) {
				this.xStick = YAW;
				this.yStick = PITCH;
				this.x2Stick = ROLL;
				this.y2Stick = T_Z;
            } else if (mode == JoystickPaneMode.ROVER) {
                this.xStick = ROVER_X;
                this.yStick = ROVER_Y;
                this.x2Stick = ROLL;
                this.y2Stick = PITCH;

                SetFlightUIMode(FlightUIMode.DOCKING);
                InputBinding.linRotState = false; // LIN mode
                switchToStagingMode = false;
            } else if (mode == JoystickPaneMode.EVA_PACK) {
                this.xStick = EVA_X;
                this.yStick = EVA_Y;
                this.x2Stick = EVA_YAW;
                this.y2Stick = EVA_Z;

                switchToStagingMode = false;
//            } else if (mode == JoystickPaneMode.EVA_WALK) {
//                this.xStick = EVA_WALK_Y;
//                this.yStick = EVA_YAW;
//                this.x2Stick = EVA_YAW;
//                this.y2Stick = EVA_Z;
//
//                switchToStagingMode = false;
			} else {
				this.xStick = ROLL;
				this.yStick = PITCH;
				this.x2Stick = YAW;
				this.y2Stick = T_Z;
			}

            if (switchToStagingMode && (FlightUIModeController.Instance.Mode != FlightUIMode.ORBITAL))
            {
                SetFlightUIMode(FlightUIMode.STAGING);
            }
		}

        static void SetFlightUIMode(FlightUIMode mode)
        {
            if (FlightGlobals.ActiveVessel != null)
            {
                FlightUIModeController.Instance.SetMode(mode);
            }
        }

//		bool ShowY2()
//		{
//            return paneMode == JoystickPaneMode.TRANSLATE || IsEVA();
//		}

        public void flyit(Vessel currentVessel, FlightCtrlState state, EVAControlState estate)
		{
            estate.resetForFlyit();

            bool keyboardTruncated = false;
			if (keyboardLimit < 1f && !input.alt) {
                foreach (var stick in ALL_AXIS)
                {
                    keyboardTruncated |= stick.handleKeyPos(state, estate, keyboardLimit);
                }
			}
            bool weAreControlling = false;

			if (input.alt) {
				if (input.x.active) {
					xStick.setTrim(input.x.v);
				}
				if (input.y.active) {
					yStick.setTrim(input.y.v);
				}
				if (input.x2.active) {
					x2Stick.setTrim(input.x2.v);
				}
				if (input.y2.active) {
					y2Stick.setTrim(input.y2.v);
				}
			} else {
				if (input.x.active) {
                    weAreControlling |= input.x.v != 0f;
					xStick.setInto(state, estate, input.x.v, false);
				}
				if (input.y.active) {
                    weAreControlling |= input.y.v != 0f;
                    yStick.setInto(state, estate, input.y.v, false);
				}
				if (input.x2.active) {
                    weAreControlling |= input.x2.v != 0f;
                    x2Stick.setInto(state, estate, input.x2.v, false);
				}
				if (input.y2.active) {
                    weAreControlling |= input.y2.v != 0f;
                    y2Stick.setInto(state, estate, input.y2.v, false);
				}
			}

            if (weAreControlling)
            {
                if (!wereWeControlling)
                {
                    // remember sas status, turn off SAS if it is on
                    restoreSAS = FlightGlobals.ActiveVessel.ActionGroups[KSPActionGroup.SAS];
                    if (restoreSAS)
                    {
                        FlightInputHandler.state.killRot = !FlightInputHandler.state.killRot;
                        FlightGlobals.ActiveVessel.ActionGroups.ToggleGroup(KSPActionGroup.SAS);
                    }
                }
            }
            else
            {
                if (wereWeControlling && this.restoreSAS != FlightGlobals.ActiveVessel.ActionGroups[KSPActionGroup.SAS])
                {
                    FlightInputHandler.state.killRot = !FlightInputHandler.state.killRot;
                    FlightGlobals.ActiveVessel.ActionGroups.ToggleGroup(KSPActionGroup.SAS);
                }
                restoreSAS = false;
            }
            wereWeControlling = weAreControlling;
		}

		public bool inside(Vector2 mousePosition)
		{
            var cursors = cursorMap[getStickMode()];

            return xyRect.Contains(mousePosition) ||
            ((cursors.x2 != null) && x2Rect.Contains(mousePosition)) ||
            ((cursors.y2 != null) && y2Rect.Contains(mousePosition));
		}

        private bool _wasFullScreen = false;
		public void Update()
		{
            if (_wasFullScreen != ourstate.fullscreen)
            {
                setDisplayMode();
                _wasFullScreen = ourstate.fullscreen;
            }
		}

        public void resetSize() {
			xyRect.size = new Vector2();
			x2Rect.size = new Vector2();
			y2Rect.size = new Vector2();
		}

        bool isMouse(EventType type) {
            if (type == EventType.MouseMove || type == EventType.MouseDown || type == EventType.MouseUp)
            {
                return true;
            }
            if (type == EventType.MouseDrag && !IsEVA())
            {
                return true; // in EVA mode, don't consider drags
            }
			return false;
		}

        public void setDisplayMode() {
            animEndTime = null;
            guiAlpha = 1;

            if (ourstate.fullscreen)
            {
                animStartTime = Time.time;
                animEndTime = animStartTime + 2f;
            }
            resetSize();
        }


        public void onGUI(FlightCtrlState state, EVAControlState estate) {
			int controlID = GUIUtility.GetControlID(FocusType.Passive);
            bool fullscreen = ourstate.fullscreen;
            bool smallSize = ourstate.smallMode;

            if (ourstate.fullscreen)
            {
                minsize = new Vector2(Screen.width * ourstate.fullscreenSize, Screen.height * ourstate.fullscreenSize);
            }
            else if (smallSize)
            {
                minsize = new Vector2(150, 150);
            }
            else
            {
                minsize = new Vector2(300, 300);
            }


			Event e = Event.current;
			EventType etype = e.GetTypeForControl(controlID);  // use rawtype, as it may be out of window
			Vector2 mousePosition = e.mousePosition;

            bool gestureOn;
            bool gestureOff;
            if (fullscreen)
            {
                gestureOff = Input.GetMouseButton(0) || Input.GetMouseButton(1) || Input.GetMouseButton(2);
                gestureOn = !gestureOff;
            }
            else
            {
                gestureOn = etype == EventType.MouseDown && e.button == 0;
                gestureOff = etype == EventType.MouseUp && e.button == 0;
            }

            if (gestureOn) {
				input.x.active = xyRect.Contains(mousePosition);
				input.y.active = input.x.active;
				input.x2.active = x2Rect.Contains(mousePosition);
				input.y2.active = y2Rect.Contains(mousePosition);
                if (!fullscreen && inside(mousePosition)) {
					GUIUtility.hotControl = controlID;
				}
			}
            if (gestureOff) {
				input.x.active = false;
				input.y.active = false;
				input.x2.active = false;
				input.y2.active = false;
                if (!fullscreen && inside(mousePosition)) {
					GUIUtility.hotControl = 0;
				}
			}
			input.alt = Event.current.alt;
            if (fullscreen || isMouse(etype)) {
				if (input.x.active || input.y.active) {
					float x = Mathf.Clamp(mousePosition.x, xyRect.x, xyRect.xMax);
					float y = Mathf.Clamp(mousePosition.y, xyRect.y, xyRect.yMax);
					input.x.v = pixToJoy(x, xyRect.x, xyRect.width);
					input.y.v = pixToJoy(y, xyRect.y, xyRect.height);
				}
				if (input.x2.active) {
					input.x2.v = pixToJoy(mousePosition.x, x2Rect.x, x2Rect.width);
				}
				if (input.y2.active) {
					input.y2.v = pixToJoy(mousePosition.y, y2Rect.y, y2Rect.height);
				}
			}

            Color origColor = GUI.color;
            bool noshow = false;
            if (animEndTime.HasValue)
            {
                float now = Time.time;
                float s = Mathf.InverseLerp(animStartTime, animEndTime.Value, now);
                guiAlpha = 1 - Mathf.Lerp(0, ourstate.minTransparency, s);

                if (Mathf.Approximately(guiAlpha, 0)) {
                    noshow = true;
                } else {
                    GUI.color = GUI.color * new Color(1,1,1, guiAlpha);
                }
            }


            if (!noshow)
            {
                var cursors = cursorMap[getStickMode()];

                if (!fullscreen)
                {
                    LayoutJoystickRects();
                }
                else
                {
                    xyRect.x = 0;
                    xyRect.y = 0;
                    xyRect.width = minsize.x;
                    xyRect.height = minsize.y;
                }
                drawKeyboardLimit(xyRect, keyboardLimit);

                drawDeadZone(xyRect, xStick, yStick);
                drawTrim(xyRect, xStick.trim(state), yStick.trim(state));

                drawCursor(xyRect, new Vector2(xStick.get(state, estate), yStick.get(state, estate)), state, estate,
                    cursors.xy
                );

                if (!fullscreen && cursors.x2 != null)
                {
                    drawDeadZone(x2Rect, x2Stick, null);
                    drawTrim(x2Rect, x2Stick.trim(state), 0);
                    drawCursor(x2Rect, new Vector2(x2Stick.get(state, estate), 0), state, estate,
                        cursors.x2
                    );
                }

                if (!fullscreen && cursors.y2 != null)
                {
                    drawDeadZone(y2Rect, null, y2Stick);
                    drawTrim(y2Rect, 0, y2Stick.trim(state));
                    drawCursor(y2Rect, new Vector2(0, y2Stick.get(state, estate)), state, estate,
                        cursors.y2
                    );
                }

                if (!fullscreen)
                {
                    textMarkers();
                }
            }
            GUI.color = origColor;
		}

        void LayoutJoystickRects()
        {
            Event e = Event.current;
            var cursors = cursorMap[getStickMode()];

            int margin = 12;
            GUILayout.BeginVertical();
            {
                GUILayout.Space(margin);
                GUILayout.BeginHorizontal();
                {
                    GUILayout.BeginVertical();
                    {
                        GUIStyle style = new GUIStyle(GUI.skin.box);
                        style.alignment = TextAnchor.UpperLeft;
                        GUILayout.Box("", style, GUILayout.MinWidth(minsize.x), GUILayout.MinHeight(minsize.y));
                        if (e.type == EventType.Repaint)
                        {
                            xyRect = GUILayoutUtility.GetLastRect();
                        }
                        GUILayout.Space(margin);
                        if (cursors.x2 != null)
                        {
                            GUILayout.Space(margin);
                            GUILayout.Box("", GUILayout.MinWidth(minsize.x), GUILayout.MinHeight(ourstate.deadRadius * 3));
                            if (e.type == EventType.Repaint)
                            {
                                x2Rect = GUILayoutUtility.GetLastRect();
                            }
                        }
                    }
                    GUILayout.EndVertical();
                    GUILayout.Space(margin);
                    if (cursors.y2 != null)
                    {
                        GUILayout.Box("", GUILayout.MinWidth(ourstate.deadRadius * 3), GUILayout.Height(minsize.y));
                        if (e.type == EventType.Repaint)
                        {
                            y2Rect = GUILayoutUtility.GetLastRect();
                        }
                        GUILayout.Space(margin);
                    }
                    else
                    {
                        y2Rect = new Rect();
                    }
                }
                GUILayout.EndHorizontal();
                GUILayout.Space(margin);
            }
            GUILayout.EndVertical();
        }

        void drawCursor(Rect rect, Vector2 rtarget, 
            FlightCtrlState state, EVAControlState estate,
            CursorInfo cinfo)
        {
            float resize = 0.7f;
            Color origColor = GUI.color;
            Matrix4x4 orig = GUI.matrix;

            GUI.color = new Color(0.6f, 0.6f, 0.7f, guiAlpha*guiAlpha);

            StickAxis pitchAxis = cinfo.pitch;
            StickAxis yawAxis = cinfo.yaw;
            StickAxis rollAxis = cinfo.roll;
            TextureCursor cursor_1 = cinfo.cursors[0];
            TextureCursor cursor_2 = cinfo.cursors.Length > 1 ? cinfo.cursors[1] : null;
            TextureCursor cursor_3 = cinfo.cursors.Length > 2 ? cinfo.cursors[2] : null;

            if (cursor_2 == null)
            {
                pitchAxis = null;
            }
            if (cursor_3 == null)
            {
//                pitchAxis = null;
                yawAxis = null;
            }

            float rx = yawAxis != null? yawAxis.get(state, estate) : 0;
            float ry = pitchAxis != null ? pitchAxis.get(state, estate) : 0;
            float rroll = rollAxis != null ? rollAxis.get(state, estate) : 0;
            float rot = 45 * rroll;

            Vector2 target = target2Pix(rect, rtarget.x, rtarget.y);

            if (Mathf.Abs(ry) > 0) {
                // reduce sheer to the width of cursor_3, minus half the "height"
                float xsheer = rx;
                if (cursor_3 != null)
                {
                    xsheer = rx * (cursor_3.texture.width + cursor_3.texture.height) / (cursor_2.texture.width);
                }
                SheerAroundPivot(1*resize, ry*resize, -xsheer*resize, 0, rot, target);
                GUI.matrix *= Translate4x4(new Vector3(target.x, target.y, 0));
                GUI.matrix *= Translate4x4(new Vector3(-cursor_2.offset.x, -cursor_2.offset.y, 0));

                Rect r = new Rect(0,0, cursor_2.texture.width, cursor_2.texture.height);
                GUI.DrawTexture(r, cursor_2.texture);
            }
            GUI.matrix = orig;

            if (Mathf.Abs(rx) > 0) {
                SheerAroundPivot(rx*resize, 1*resize, 0, -ry*resize, rot, target);
                GUI.matrix *= Translate4x4(new Vector3(target.x, target.y, 0));
                GUI.matrix *= Translate4x4(new Vector3(-cursor_3.offset.x, -cursor_3.offset.y, 0));

                Rect r = new Rect(0,0, cursor_3.texture.width, cursor_3.texture.height);
                GUI.DrawTexture(r, cursor_3.texture);
            }
            GUI.matrix = orig;

            if (cursor_1 != null)
            {
                SheerAroundPivot(1*resize, 1*resize, 0, 0, rot, target);
                GUI.matrix *= Translate4x4(new Vector3(target.x, target.y, 0));
                GUI.matrix *= Translate4x4(new Vector3(-cursor_1.offset.x, -cursor_1.offset.y, 0));
                Rect r = new Rect(0,0, cursor_1.texture.width, cursor_1.texture.height);
                GUI.DrawTexture(r, cursor_1.texture);
            }
            GUI.matrix = orig;
            GUI.color = origColor;
        }

        void SheerAroundPivot(float xscale, float yscale, float xsheer, float ysheer, float rot, Vector2 target)
        {
            Vector2 vector2 = GUIUtility.GUIToScreenPoint(target);
            GUI.matrix = 
                Matrix4x4.TRS((Vector3) vector2, Quaternion.Euler(0.0f, 0.0f, rot),new Vector3(xscale, yscale, 1f)) * 
                Sheer4x4(xsheer, ysheer) *
                Translate4x4((Vector3)(-vector2)) * 
                GUI.matrix;
        }

        Matrix4x4 Rotate4x4(float rot)
        {
            float s = Mathf.Sin(rot);
            float c = Mathf.Sin(rot);
            Matrix4x4 m = Matrix4x4.identity;
            m[0, 0] = c;
            m[0, 1] = -s;
            m[1, 0] = s;
            m[1, 1] = c;
            return m;
        }

        public static Matrix4x4 Translate4x4(Vector3 t)
        {
            Matrix4x4 m = Matrix4x4.identity;
            m[0, 3] = t.x;
            m[1, 3] = t.y;
            m[2, 3] = t.z;
            return m;
        }
        static Matrix4x4 Sheer4x4(float x, float y)
        {
            Matrix4x4 m = Matrix4x4.identity;
            m[0, 1] = x;
            m[1, 0] = y;
            return m;
        }

		void textMarkers()
		{
//			// primary zone
            drawXTextMarkers(xyRect, xStick);
            drawYTextMarkers(xyRect, yStick);

            var cursors = cursorMap[getStickMode()];
			// x2 axis
            if (cursors.x2 != null)
            {
                drawXTextMarkers(x2Rect, x2Stick);
            }
			
			// y2 axis
            if (cursors.y2 != null) {
                drawYTextMarkers(y2Rect, y2Stick);
			}
		}

        void drawXTextMarkers(Rect rect, StickAxis stick)
        {
            drawTextMarker(new Vector2(rect.x, rect.center.y), stick.negKey(), false, true);
            drawTextMarker(new Vector2(rect.xMax, rect.center.y), stick.posKey(), false, false);
        }

        void drawYTextMarkers(Rect rect, StickAxis stick)
        {
            drawTextMarker(new Vector2(rect.center.x, rect.y), stick.negKey(), true, true);
            drawTextMarker(new Vector2(rect.center.x, rect.yMax), stick.posKey(), true, false);
        }

        void drawTextMarker(Vector2 pos, string kb, bool vert, bool neg) {

            GUIStyle tstyle = new GUIStyle(GUI.skin.label);
            tstyle.fontSize = kb.Length == 1 ? 12 : 10;
            tstyle.fontStyle = FontStyle.Bold;
            tstyle.normal.textColor = new Color(0.6f, 0.6f, 0.7f, guiAlpha);

            tstyle.alignment = TextAnchor.MiddleCenter;
            float baseline = 3; // guess

            GUIContent content = new GUIContent(kb);
            var size = tstyle.CalcSize(content);
            var r = new Rect(pos.x, pos.y, size.x, size.y);

            if (vert)
            {
                r.x -= r.width / 2;
                if (!neg)
                {
//                    r.y += r.height;
                }
                else
                {
                    r.y -= r.height - baseline;
                }
            }
            else
            {
                r.y -= r.height/2 - baseline;
                if (!neg)
                {
                    r.x -= r.width;
                }
            }

            tstyle.Draw(r, content, false, false, false, false);
        }

		float pixToJoy(float v, float of, float w)
		{
            float r = ourstate.deadRadius;
			float u;
			if (v < (of + r)) u = -1f;
			else if (v <= (of + w/2 - r)) u = -((of + w/2 - r) - v) / (w/2 - 2 * r);
			else if (v < (of + w/2 + r)) u = 0f;
			else if (v <= (of + w - r)) u = (v - (of + w/2 + r)) / ( w/2 - 2 * r);
			else u = 1f;

            // x^2 response curve
            float u2 = u * u;
            return u < 0f ? -u2 : u2;
		}

        float joyToPix(float u2, float of, float w) {
            // x^2 response curve
            float u  = u2 < 0f ? -Mathf.Sqrt(-u2) : Mathf.Sqrt(u2);

            float r = ourstate.deadRadius;
			float v;
			if (u == -1f) v = of;
			else if (u < 0f) v = (of + w/2 - r) + u * (w/2 - 2*r);
			else if (u == 0f) v = of + w/2;
			else if (u < 1f) v = (of + w/2 + r) + u * (w/2 - 2*r);
			else v = of + w;

			return v;
		}

		static float zeroSmall(float v, float eps)
		{
			return (Mathf.Abs(v) < eps) ? 0f : v;
		}

		void drawKeyboardLimit(Rect rect, float keyboardLimit)
		{
			if (keyboardLimit < 1f) {
                Color c = new Color(0.9f, 0.6f, 0.6f, guiAlpha);
				float x0 = joyToPix(-keyboardLimit, rect.x, rect.width);
				float x1 = joyToPix(+keyboardLimit, rect.x, rect.width);
				float y0 = joyToPix(-keyboardLimit, rect.y, rect.height);
				float y1 = joyToPix(+keyboardLimit, rect.y, rect.height);
				Drawing.DrawRect(Rect.MinMaxRect(x0, y0, x1, y1), c, 1);
			}
		}

        void drawDeadZone(Rect rect, StickAxis xaxis, StickAxis yaxis)
		{
            float deadRadius = ourstate.deadRadius;
            // dead zone (4 squares)
            Color c = new Color(0.6f, 0.6f, 0.7f, guiAlpha*guiAlpha*guiAlpha);
			float x0 = rect.x;
			float y0 = rect.y;
			float x1 = rect.x + rect.width/2;
			float y1 = rect.y + rect.height/2;
			float x2 = rect.x + rect.width;
			float y2 = rect.y + rect.height;
			float width = 1;

            if (yaxis != null && yaxis != null)
            {
                Drawing.DrawRect(Rect.MinMaxRect(x0 + deadRadius, y0 + deadRadius, x1 - deadRadius, y1 - deadRadius), c, width);
                Drawing.DrawRect(Rect.MinMaxRect(x1 + deadRadius, y1 + deadRadius, x2 - deadRadius, y2 - deadRadius), c, width);
                Drawing.DrawRect(Rect.MinMaxRect(x0 + deadRadius, y1 + deadRadius, x1 - deadRadius, y2 - deadRadius), c, width);
                Drawing.DrawRect(Rect.MinMaxRect(x1 + deadRadius, y0 + deadRadius, x2 - deadRadius, y1 - deadRadius), c, width);
                if (ourstate.fullscreen)
                {
                    c = new Color(0.5f, 0.9f, 0.5f, guiAlpha); 
                    width = 2;
                    float len = Math.Min(rect.width, rect.height) * 0.05f;
                    // center hlines
                    Drawing.DrawHLine(x1 - deadRadius - len, x1 - deadRadius, y1 - deadRadius, c, width);
                    Drawing.DrawHLine(x1 + deadRadius, x1 + deadRadius + len, y1 - deadRadius, c, width);
                    Drawing.DrawHLine(x1 - deadRadius - len, x1 - deadRadius, y1 + deadRadius, c, width);
                    Drawing.DrawHLine(x1 + deadRadius, x1 + deadRadius + len, y1 + deadRadius, c, width);
                    // center vlines
                    Drawing.DrawVLine(y1 - deadRadius - len, y1 - deadRadius, x1 - deadRadius, c, width);
                    Drawing.DrawVLine(y1 + deadRadius, y1 + deadRadius + len, x1 - deadRadius, c, width);
                    Drawing.DrawVLine(y1 - deadRadius - len, y1 - deadRadius, x1 + deadRadius, c, width);
                    Drawing.DrawVLine(y1 + deadRadius, y1 + deadRadius + len, x1 + deadRadius, c, width);

//                    c.a = guiAlpha*guiAlpha; 
                    width = 2;
                    // outer hlines
                    Drawing.DrawHLine(x0, x0 + len, y0, c, width); 
                    Drawing.DrawHLine(x2 - len, x2, y0, c, width); 
                    Drawing.DrawHLine(x0, x0 + len, y2, c, width); 
                    Drawing.DrawHLine(x2 - len, x2, y2, c, width);
                    Drawing.DrawHLine(x1 - len, x1 + len, y0, c, width);
                    Drawing.DrawHLine(x1 - len, x1 + len, y2, c, width);
                    // outer vlines
                    Drawing.DrawVLine(y0, y0 + len, x0, c, width);
                    Drawing.DrawVLine(y2 - len, y2, x0, c, width);
                    Drawing.DrawVLine(y0, y0 + len, x2, c, width);
                    Drawing.DrawVLine(y2 - len, y2, x2, c, width);
                    Drawing.DrawVLine(y1 - len, y1 + len, x0, c, width);
                    Drawing.DrawVLine(y1 - len, y1 + len, x2, c, width);
                }
            }
            else if (yaxis != null)
            {
                Drawing.DrawHLine(x0, x2, y1 - deadRadius, c, width);
                Drawing.DrawHLine(x0, x2, y1 + deadRadius, c, width);
            }
            else if (xaxis != null)
            {
                Drawing.DrawVLine(y0, y2, x1 - deadRadius, c, width);
                Drawing.DrawVLine(y0, y2, x1 + deadRadius, c, width);
            }
		}

        const float sasJitterThres = 0.01f; // stop jittering around when SAS produces very small adjustments 

		private void drawTrim(Rect rect, float trimx, float trimy)
		{
            float deadRadius = ourstate.deadRadius;
			if ((Mathf.Abs(trimx) > 0)) {
				float m = rect.y + rect.height/2;
				float tx = joyToPix(trimx, rect.x, rect.width);
                Drawing.DrawVLine(m - deadRadius, m+deadRadius, tx, MyResources.trimColor, 3);
			}
			if ((Mathf.Abs(trimy) > 0)) {
				float m = rect.x + rect.width/2;
				float ty = joyToPix(trimy, rect.y, rect.height);
                Drawing.DrawHLine(m - deadRadius, m+deadRadius, ty, MyResources.trimColor, 3);
			}

		}
        private Vector2 target2Pix(Rect rect, float rx, float ry) {
            float jitter = IsManualOverride() ? 0f : sasJitterThres;
            float x = joyToPix(zeroSmall(rx, jitter), rect.x, rect.width);
            float y = joyToPix(zeroSmall(ry, jitter), rect.y, rect.height);
            return new Vector2(x, y);
        }
	}
}

