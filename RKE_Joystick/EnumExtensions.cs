﻿using System;

namespace RKE_Joystick
{
    public static class EnumExtensions
    {
        public static bool IsFlagSet<T>(this T value, T flag) where T : struct
        {
            long v = Convert.ToInt64(value);
            long f = Convert.ToInt64(flag);
            return (v & f) != 0;
        }
    }
}

