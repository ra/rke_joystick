**Runge–Kutta Enterprises presents...**
## **On-screen Joystick** (and Fine Keyboard Controls)

* **Purpose**: A quality-of-life mod to give you fine-grain control of your craft. Like an analogue joystick, but on-screen. This plugin grew out of my frustration trying to do fine pitch adjustment with my spaceplanes.
* **More info:** [Forum Thread](http://forum.kerbalspaceprogram.com/threads/92586)

### Images
[(More in album)](http://imgur.com/a/KUQ1u)

![](http://i.imgur.com/qGw6g8s.png)

### Video run through
[[https://www.youtube.com/watch?v=yyKabJs3M8M]]

